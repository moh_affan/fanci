<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
	<div class="col-md-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h2 class="text-center text-primary">Sistem Aplikasi Pengajuan Dokumen Hukum</h2>
				<h4 class="text-center">Bagian Hukum Sekretariat Daerah</h4>
				<h3 class="text-center text-success">Kabupaten Sumenep</h3>
				<img class="mx-auto d-block" src="<?= site_url('assets/themes/cloudui/images/sumenep.png') ?>"
					 alt="logo"/>
			</div>
		</div>
	</div>
	<div class="col-md-4 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Peraturan Daerah</h4>
				<div class="row">
					<div class="col-md-6">
						<p class="text-center">Jumlah Pengajuan</p>
						<div class="text-center">
							<span class="text-primary font-weight-bold h2">10</span> <sub>item</sub>
						</div>
					</div>
					<div class="col-md-6">
						<p class="text-center">Selesai Diproses</p>
						<div class="text-center">
							<span class="text-success font-weight-bold h2">10</span> <sub>item</sub>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Peraturan Bupati</h4>
				<div class="row">
					<div class="col-md-6">
						<p class="text-center">Jumlah Pengajuan</p>
						<div class="text-center">
							<span class="text-primary font-weight-bold h2">10</span> <sub>item</sub>
						</div>
					</div>
					<div class="col-md-6">
						<p class="text-center">Selesai Diproses</p>
						<div class="text-center">
							<span class="text-success font-weight-bold h2">10</span> <sub>item</sub>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">Keputusan Bupati</h4>
				<div class="row">
					<div class="col-md-6">
						<p class="text-center">Jumlah Pengajuan</p>
						<div class="text-center">
							<span class="text-primary font-weight-bold h2">10</span> <sub>item</sub>
						</div>
					</div>
					<div class="col-md-6">
						<p class="text-center">Selesai Diproses</p>
						<div class="text-center">
							<span class="text-success font-weight-bold h2">10</span> <sub>item</sub>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
