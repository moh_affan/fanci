<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$ci =& get_instance();
$pengguna = $ci->session->userdata('identity');
$user_id = $ci->ion_auth->get_user_id();
?>

<div class="nav-bottom">
	<div class="container">
		<ul class="nav page-navigation">
			<li class="nav-item dashboard">
				<a href="<?= site_url('admin/dashboard') ?>" class="nav-link" style="padding-left: 18px">
					<i class="link-icon icon-home"></i></a>
			</li>
			<li class="nav-item mega-menu master">
				<a href="#" class="nav-link"><i class="link-icon icon-drawer"></i>
					<span class="menu-title">Master</span><i class="menu-arrow"></i></a>
				<div class="submenu">
					<div class="col-group-wrapper row">
						<div class="col-group col-md-3">
							<p class="category-heading">User Pages</p>
							<ul class="submenu-item">
								<li class="nav-item"><a class="nav-link" href="pages/samples/login.html">Login</a></li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/login-2.html">Login 2</a>
								</li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/register.html">Register</a>
								</li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/register-2.html">Register
										2</a></li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/lock-screen.html">Lockscreen</a>
								</li>
							</ul>
						</div>
						<div class="col-group col-md-3">
							<p class="category-heading">Error Pages</p>
							<ul class="submenu-item">
								<li class="nav-item"><a class="nav-link" href="pages/samples/error-400.html">400</a>
								</li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/error-404.html">404</a>
								</li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/error-500.html">500</a>
								</li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/error-505.html">505</a>
								</li>
							</ul>
						</div>
						<div class="col-group col-md-3">
							<p class="category-heading">E-commerce</p>
							<ul class="submenu-item">
								<li class="nav-item"><a class="nav-link" href="pages/samples/invoice.html">Invoice</a>
								</li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/pricing-table.html">Pricing
										Table</a></li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/orders.html">Orders</a>
								</li>
							</ul>
						</div>
						<div class="col-group col-md-3">
							<p class="category-heading">General</p>
							<ul class="submenu-item">
								<li class="nav-item"><a class="nav-link" href="pages/samples/search-results.html">Search
										Results</a></li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/profile.html">Profile</a>
								</li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/timeline.html">Timeline</a>
								</li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/news-grid.html">News
										grid</a></li>
								<li class="nav-item"><a class="nav-link"
														href="pages/samples/portfolio.html">Portfolio</a></li>
								<li class="nav-item"><a class="nav-link" href="pages/samples/faq.html">FAQ</a></li>
							</ul>
						</div>
					</div>
				</div>
			</li>
			<li class="nav-item pengajuan">
				<a href="<?= site_url('admin/pengajuan') ?>" class="nav-link"><i class="link-icon icon-list"></i><span
						class="menu-title">Pengajuan</span><i
						class="menu-arrow"></i></a>
				<div class="submenu">
					<ul class="submenu-item">
						<li class="nav-item"><a class="nav-link" href="<?= site_url('admin/pengajuan?perda=1') ?>">Peraturan
								Daerah</a></li>
						<li class="nav-item"><a class="nav-link" href="<?= site_url('admin/pengajuan?perbup=1') ?>">Peraturan
								Bupati</a></li>
						<li class="nav-item"><a class="nav-link" href="<?= site_url('admin/pengajuan?kepbup=1') ?>">Keputusan
								Bupati</a></li>
					</ul>
				</div>
			</li>
			<li class="nav-item mgmt-user">
				<a href="#" class="nav-link"><i class="link-icon icon-user"></i>
					<span class="menu-title">Manajemen Pengguna</span>
					<i class="menu-arrow"></i></a>
				<div class="submenu">
					<ul class="submenu-item">
						<li class="nav-item group"><a class="nav-link" href="<?= site_url('admin/pengguna/grup') ?>">Grup</a></li>
						<li class="nav-item users"><a class="nav-link" href="<?= site_url('admin/pengguna') ?>">Pengguna</a></li>
					</ul>
				</div>
			</li>
			<li class="nav-item"><?php for ($i = 0; $i < 50; $i++) echo '&nbsp;' ?></li>
			<li class="nav-item"><?php for ($i = 0; $i < 100; $i++) echo '&nbsp;' ?></li>
		</ul>
	</div>
</div>
